locals {
  tags = {
    "managed-by" = "terraform"
    "source"     = "https://gitlab.com/VERRONS/jwt-ctf/"
    "project"    = "jwt-misconfiguration-workshop"
  }
}

module "lambda_function_none" {
  source = "terraform-aws-modules/lambda/aws"

  function_name                     = "jwt-none-alg-${random_pet.this.id}"
  description                       = "Vulnerable jwt function with weak secret"
  handler                           = "none-alg.lambda_handler"
  runtime                           = "python3.8"
  cloudwatch_logs_retention_in_days = 30
  publish                           = true
  tags                              = local.tags

  source_path = "./src/none-alg"

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${module.api_gateway.apigatewayv2_api_execution_arn}/*/*"
    }
  }
}

module "lambda_function_weak_secret" {
  source = "terraform-aws-modules/lambda/aws"

  function_name                     = "jwt-weak-secret-${random_pet.this.id}"
  description                       = "Vulnerable jwt function with weak secret"
  handler                           = "weak-secret.lambda_handler"
  runtime                           = "python3.8"
  cloudwatch_logs_retention_in_days = 30
  publish                           = true
  tags                              = local.tags

  source_path = "./src/weak-secret"

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${module.api_gateway.apigatewayv2_api_execution_arn}/*/*"
    }
  }
}

module "lambda_function_claim_verification" {
  source = "terraform-aws-modules/lambda/aws"

  function_name                     = "jwt-claim-verification-${random_pet.this.id}"
  description                       = "Vulnerable jwt function with bad claim verification"
  handler                           = "claim-verification.lambda_handler"
  runtime                           = "python3.8"
  cloudwatch_logs_retention_in_days = 30
  publish                           = true
  tags                              = local.tags

  source_path = "./src/claim-verification"

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${module.api_gateway.apigatewayv2_api_execution_arn}/*/*"
    }
  }
}

module "lambda_function_key_confusion" {
  source = "terraform-aws-modules/lambda/aws"

  function_name                     = "jwt-key-confusion-${random_pet.this.id}"
  description                       = "Vulnerable jwt function with weak secret"
  handler                           = "key-confusion.lambda_handler"
  runtime                           = "python3.8"
  cloudwatch_logs_retention_in_days = 30
  publish                           = true
  tags                              = local.tags

  source_path = "./src/key-confusion"

  allowed_triggers = {
    AllowExecutionFromAPIGateway = {
      service    = "apigateway"
      source_arn = "${module.api_gateway.apigatewayv2_api_execution_arn}/*/*"
    }
  }
}

module "api_gateway" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name                   = "jwt-all"
  description            = "JWT Misconfiguration workshop"
  protocol_type          = "HTTP"
  create_api_domain_name = false
  tags                   = local.tags

  default_route_settings = {
    detailed_metrics_enabled = true
    throttling_burst_limit   = 100
    throttling_rate_limit    = 100
  }

  default_stage_access_log_destination_arn = aws_cloudwatch_log_group.logs.arn
  default_stage_access_log_format          = "$context.identity.sourceIp - - [$context.requestTime] \"$context.httpMethod $context.routeKey $context.protocol\" $context.status $context.responseLength $context.requestId $context.integrationErrorMessage"

  integrations = {
    # None alg
    "ANY /" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_none.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /admin" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_none.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /user" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_none.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    # Weak secret
    "ANY /f91c2979fd539a16c2098c812a9973ab" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_weak_secret.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /f91c2979fd539a16c2098c812a9973ab/admin" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_weak_secret.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /f91c2979fd539a16c2098c812a9973ab/user" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_weak_secret.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    # Bad claim verification
    "ANY /03f21b939e022176e87feb7704477bbc" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_claim_verification.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /03f21b939e022176e87feb7704477bbc/admin" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_claim_verification.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /03f21b939e022176e87feb7704477bbc/storage" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_claim_verification.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /03f21b939e022176e87feb7704477bbc/storage_token" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_claim_verification.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    # Key confusion
    "ANY /ddadb4ba73571b14e35bcb53df73af46" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_key_confusion.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /ddadb4ba73571b14e35bcb53df73af46/admin" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_key_confusion.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /ddadb4ba73571b14e35bcb53df73af46/pub" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_key_confusion.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    },
    "ANY /ddadb4ba73571b14e35bcb53df73af46/user" = {
      integration_type       = "AWS_PROXY"
      integration_method     = "POST"
      lambda_arn             = module.lambda_function_key_confusion.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
  }
}

resource "random_pet" "this" {
  length = 3
}

resource "aws_cloudwatch_log_group" "logs" {
  name              = "jwt-vulnerability-${random_pet.this.id}"
  retention_in_days = 30
}
