"""Python AWS Lambda Create token for the challenge 1
   This example Lambda function will simply return a JWT and
   a HTTP Status Code 200.
"""

import json
import jwt 
import base64

def user(username):
    if(not username == ""):
        payload = {"username": username, "is_admin": False}
        priv_pem_file=open('key.pem','rb')
        priv_pem = priv_pem_file.read()
        token = jwt.encode(payload, priv_pem, algorithm="RS256")
        return token
    else:
        return "Invalid username"
### function to redifine prepare key method to disable checking for asymmetric key words
def prepare_key(key):
    return jwt.utils.force_bytes(key)
### verify if my token is well set up with the scope admin at true
def verify_token_1(token, pub_pem, alg): 
    status = False
    try:
        payload = jwt.decode(token, pub_pem, alg)
        print(payload)
        try : 
            status = payload['is_admin']
            print("Status is: " + str(status))
            if(status == True):
                body = '{ You win this challenge Congratulation }'
            else : 
                body = '{ You are so close ... }'
        except Exception as e: 
            body = '{ You are on the right way }' + e
    except jwt.exceptions.InvalidSignatureError:
        body='{ Wrong signature }'
    except:
        body = '{ You made a mistake, I will not give you more information }'
    return body

### check if the signature is correct    
def verify_token_signature(token,pub_pem,alg):
    try :    
        token = jwt.decode(token, pub_pem, alg)
        return True
    except:  
      return False

def lambda_handler(event, context):
    print(json.dumps(event))
    path = event['requestContext']['http']['path']
    method = event['requestContext']['http']['method']       

    # GET method
    if(method == 'GET'):
        if(path == '/ddadb4ba73571b14e35bcb53df73af46'):
            body = 'Hi welcome to the most most most diy secure JWT implementation!! This time I used military grade RSA encryption ! You can POST {"username": "example"} to /user and have the public key in /pub, but you will never be able to authentiate as admin in /admin'
        elif(path == '/ddadb4ba73571b14e35bcb53df73af46/admin' or path == '/user'):
            body = "POST not GET ;)"
        elif( path == '/ddadb4ba73571b14e35bcb53df73af46/pub'):
            pub_pem_file=open('public.pem','r')
            body = {'PubKey':json.dumps(pub_pem_file.read())}
        else:
            body = {'message': 'Invalid path'}

    # POST method
    elif(method == 'POST'):
        # Decode payload and parse JSON
        try:
            payload = json.loads(base64.b64decode(event['body']))
            print(json.dumps(payload))
        except Exception as e:
            body = "Error decoding payload: " + str(e)

        if(path == '/ddadb4ba73571b14e35bcb53df73af46/user'):
            try:
                body = {"token": user(payload['username'])}
            except Exception as e:
                body = "Error on /user: " + str(e)
        elif(path == '/ddadb4ba73571b14e35bcb53df73af46/admin'):
            try:
                print("Token is: " + event['headers']['authorization'])
                pub_pem_file=open('public.pem','rb')
                pub_pem = pub_pem_file.read()
                verify_toke_signature_bool = verify_token_signature(event['headers']['authorization'],pub_pem,['RS256'])
                if (not verify_toke_signature_bool) :
                    # override HS256's prepare key method to disable checking for asymmetric key words
                    jwt.api_jws._jws_global_obj._algorithms['HS256'].prepare_key = prepare_key
                    body = verify_token_1(event['headers']['authorization'],pub_pem,['HS256'])
                else:
                    
                     body=verify_token_1(event['headers']['authorization'],pub_pem,['RS256'])
            except Exception as e:
                body = "Error on /admin, missing 'Authorization' header ? " + str(e)
        else:
            body = {'message': 'Invalid path'}
    else:
        body = {'message': 'Invalid HTTP method'}
    
    response = {
        'statusCode': 200,
        'body': json.dumps(body)
    }
    
    return response