# How to get it work on AWS Lambda
To get this function to work on AWS Lambda you must install python deps inside a Lmanda docker container.

## Launch docker container

```bash
cd src/key-confusion
docker run --rm -it -v $(pwd):/mnt --entrypoint /bin/bash amazon/aws-lambda-python
```

yum install gcc
pip install wheel
pip install pyjwt cryptography cffi -t .