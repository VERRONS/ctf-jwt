import jwt
import json
import base64

secret_key = "y1KLVPGNPjR2rNwVaYxin_9y-ilHapf9nFmpovGh"

def storage_token(username):
    payload = {"username": username, "feature":"storage", "group":"admin"}
    token = jwt.encode(payload, secret_key, algorithm="HS256")
    return token

def check_token_storage(token):
    try:
        payload = jwt.decode(token, secret_key, algorithms=["HS256"])
        if(payload["group"] == "admin"):
            return payload["username"] + "! Welcome to the storage beta feature o/ Everyone is admin for the time being, we will fix this when we'll push this feature in prod"
        else:
            return "Error: user " + payload["username"] + " does not have a valid token"
    except Exception as e:
        return "Invalid token: " + str(e)

def check_token_admin(token):
    try:
        payload = jwt.decode(token, secret_key, algorithms=["HS256"])
        if(payload["group"] == "admin"):
            return "Congrats " + payload["username"] +"! That's what happens when you don't check the claims properly ;) The next challenge url is: ddadb4ba73571b14e35bcb53df73af46"
        else:
            return "Error: user " + payload["username"] + " does not have a valid token"
    except Exception as e:
        return "Invalid token: " + str(e)

def lambda_handler(event, context):
    print(json.dumps(event))
    path = event['requestContext']['http']['path']
    method = event['requestContext']['http']['method']       

    # GET method
    if(method == 'GET'):
        if(path == '/03f21b939e022176e87feb7704477bbc'):
            body = 'Hi welcome to our site ! You can test our beta feature in /storage but you will never be able to access /admin, because only admin group can get an access to it'
        elif(path == '/03f21b939e022176e87feb7704477bbc/storage'):
            body = 'Hi, please first grab a token by sending a POST request to /storage_token with your username in the payload (example: {"username": "example"}). Then come back and authenticate with your token by sending a POST request with your token in the header (example header "Authorization: XXXXXXXXXX")'
        elif(path == '/03f21b939e022176e87feb7704477bbc/admin' or path == '/03f21b939e022176e87feb7704477bbc/storage_token'):
            body = "POST not GET ;)"
        else:
            body = {'message': 'Invalid path'}

    # POST method
    elif(method == 'POST'):
        # Decode payload and parse JSON
        try:
            payload = json.loads(base64.b64decode(event['body']))
            print(json.dumps(payload))
        except Exception as e:
            body = "Error decoding payload: " + str(e)

        if(path == '/03f21b939e022176e87feb7704477bbc/storage_token'):
            try:
                body = {"token": storage_token(payload['username'])}
            except Exception as e:
                body = "Error on /storage_token: " + str(e)
        elif(path == '/03f21b939e022176e87feb7704477bbc/storage'):
            try:
                print("Token is: " + event['headers']['authorization'])
                body = check_token_storage(event['headers']['authorization'])
            except Exception as e:
                body = "Error on /storage, missing 'Authorization' header ? " + str(e)
        elif(path == '/03f21b939e022176e87feb7704477bbc/admin'):
            try:
                print("Token is: " + event['headers']['authorization'])
                body = check_token_admin(event['headers']['authorization'])
            except Exception as e:
                body = "Error on /admin, missing 'Authorization' header ? " + str(e)
        else:
            body = {'message': 'Invalid path'}
    else:
        body = {'message': 'Invalid HTTP method'}
    
    response = {
        'statusCode': 200,
        'body': json.dumps(body)
    }
    
    return response